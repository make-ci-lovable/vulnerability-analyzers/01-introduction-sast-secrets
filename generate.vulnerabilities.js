const fs = require("fs")

let start = new Date()

// ==================================
//  Severity:
//  Critical, High, Medium
// ==================================
let severity = "Critical"

// ==================================
//  Confidence:
//  Unknown Confirmed Experimental
// ==================================
let confidence = "Confirmed"

// ==================================
//  Scanner's info
// ==================================
let scannerName = "Zoo"
let scannerId = "zoo"
let scannerSrc = "https://bots.garden"
let scannerVendor = "Bots.Garden"
let scannerVersion = "0.0.1"

// ==================================
//  Vulnerabilities' list
// ==================================
let vulnerabilities = [
  {
    category: "sast",
    name: `🔴: 🐯`,
    message: `📝: 💥 we found a 🐯`,
    description: `🖐️: this is a 🐯`,
    cve: "000001",
    severity: "Critical", // Critical, High, Medium
    confidence: "Confirmed", // Unknown Confirmed Experimental
    scanner: {id: scannerId, name: scannerName},
    location: {
      file: "tiger.txt", 
      start_line: 10,
      end_line: 10,
      class: "owasp 🐯",
      method: "zoo",
      dependency: {package: {}}
    },
    identifiers: [
      {
        type: "zoo_rule_id",
        name: "Zoo rule cwe 🐯",
        value: "owasp 🐯"
      }
    ]
  },
  {
    category: "sast",
    name: `🟠: 🐱`,
    message: `📝: 💥 we found a 🐱`,
    description: `🖐️: this is a 🐱`,
    cve: "000002",
    severity: "Medium", // Critical, High, Medium
    confidence: "Experimental", // Unknown Confirmed Experimental
    scanner: {id: scannerId, name: scannerName},
    location: {
      file: "cat.txt", 
      start_line: 5,
      end_line: 5,
      class: "owasp cat",
      method: "zoo",
      dependency: {package: {}}
    },
    identifiers: [
      {
        type: "zoo_rule_id",
        name: "Zoo rule cwe 🐱",
        value: "owasp 🐱"
      }
    ]
  }                       
              
]

let end = new Date() - start

// ==================================
//  SAST Report
// ==================================
let sastReport = {
  version: "3.0",
  vulnerabilities: vulnerabilities,
  remediations: [],
  scan: {
    scanner: {
      id: scannerId,
      name: scannerName,
      url: scannerSrc,
      vendor: {
        name: scannerVendor
      },
      version: scannerVersion
    },
    type: "sast",
    start_time: start,
    end_time: end,
    status: "success"
  }
}

fs.writeFileSync("./gl-sast-report.json", JSON.stringify(sastReport, null, 2))
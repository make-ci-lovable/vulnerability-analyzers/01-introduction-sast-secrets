const fs = require("fs")

let start = new Date()

// ==================================
//  Severity:
//  Critical, High, Medium
// ==================================
let severity = "Critical"

// ==================================
//  Confidence:
//  Unknown Confirmed Experimental
// ==================================
let confidence = "Confirmed"

// ==================================
//  Scanner's info
// ==================================
let scannerName = "Secrets"
let scannerId = "secrets"
let scannerSrc = "https://bots.garden"
let scannerVendor = "Bots.Garden"
let scannerVersion = "0.0.1"

// ==================================
//  Vulnerabilities' list
// ==================================
let vulnerabilities = [
  {
    category: "secret_detection",
    name: `🔴: 😡🥵🥶`,
    message: `📝: 💥 we found a secret`,
    description: `🖐️: this is a secret`,
    cve: "000001",
    severity: "Critical", // Critical, High, Medium
    confidence: "Confirmed", // Unknown Confirmed Experimental
    scanner: {id: scannerId, name: scannerName},
    location: {
      file: "tiger.txt", 
      start_line: 10,
      end_line: 10,
      commit: {
        author: "k33g",
        message: "😜",
        date: "Sun Nov 8 13:31:56 2020 +0000",
        sha: "721f4cde83ee2da147461c0b885a0c909c5d1b19"
      }
    },
    identifiers: [
      {
        type: "k33g_secret_rule_id",
        name: "K33g rule 42",
        value: "💥"
      }
    ]
  }              
]

let end = new Date() - start

// ==================================
//  SECRET Report
// ==================================
let secretReport = {
  version: "3.0",
  vulnerabilities: vulnerabilities,
  remediations: [],
  scan: {
    scanner: {
      id: scannerId,
      name: scannerName,
      url: scannerSrc,
      vendor: {
        name: scannerVendor
      },
      version: scannerVersion
    },
    type: "secret_detection",
    start_time: start,
    end_time: end,
    status: "success"
  }
}

fs.writeFileSync("./gl-secret-detection-report.json", JSON.stringify(secretReport, null, 2))

